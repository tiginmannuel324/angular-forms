import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent{

  constructor() { }
  
  @ViewChild('f') from: NgForm;
  answer='';
  genders= ['male', 'female'];

  suggestUserName() {
    const suggestedName = 'Superuser';
    // this.from.setValue({
    //   userData: {
    //     username: suggestedName,
    //     email: ''
    //   },
    //   secret: 'pet',
    //   quesAns: '',
    //   gender: 'male'
    // });

    this.from.form.patchValue({
      userData: {
        username: suggestedName
      }
    });
  }

  // onSubmit(form: NgForm){
  //   console.log(form);
  // }

  onSubmit(){
    this.from.reset();
  }

}
