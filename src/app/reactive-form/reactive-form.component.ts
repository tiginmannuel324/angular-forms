import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  genders = ['male', 'female'];
  signupForm: FormGroup;
  forbiddenUsername=['ankur', 'ankur1'];

  constructor() { }

  ngOnInit() {
    this.signupForm= new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
        'email': new FormControl(null, [ Validators.required, Validators.email], this.forbiddenEmails)
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([])
    });

    // this.signupForm.valueChanges.subscribe(
    //   (value) => console.log(value)
    // );
    // this.signupForm.statusChanges.subscribe(
    //   (status) => console.log(status)
    // );
  }

  onAddHobby(){
    const hobby= new FormControl(null);
    (<FormArray>this.signupForm.get('hobbies')).push(hobby);
  }

  onSubmit(){
    console.log(this.signupForm);
  }

  forbiddenNames(control: FormControl):{[s:string]: boolean} {
    if( this.forbiddenUsername.indexOf(control.value) != -1){
      return {'forbidden': true};
    }
    return null;
  }

  //Asyncronous Validators
  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {
    const promise= new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if(control.value === 'ankur@a.j'){
          resolve({'email forbidden': true});
        }
        else{
          resolve(null);
        }
      },1000);
    });
    return promise
  }

}
